﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowTheDoctorDetails.Models.Slot
{
    public class Meta
    {
        public DateTime lastUpdated { get; set; }
    }

    public class Link
    {
        public string relation { get; set; }
        public string url { get; set; }
    }

    public class Meta2
    {
        public string versionId { get; set; }
        public DateTime lastUpdated { get; set; }
    }

    public class Schedule
    {
        public string reference { get; set; }
    }

    public class Resource
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta2 meta { get; set; }
        public Schedule schedule { get; set; }
        public string status { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }

    public class Search
    {
        public string mode { get; set; }
    }

    public class Entry
    {
        public string fullUrl { get; set; }
        public Resource resource { get; set; }
        public Search search { get; set; }
    }

    public class SlotModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta meta { get; set; }
        public string type { get; set; }
        public List<Link> link { get; set; }
        public List<Entry> entry { get; set; }
    }
}
