﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowTheDoctorDetails.Models.PractitionerRole
{
    public class Meta
    {
        public DateTime lastUpdated { get; set; }
    }

    public class Link
    {
        public string relation { get; set; }
        public string url { get; set; }
    }

    public class Meta2
    {
        public string versionId { get; set; }
        public DateTime lastUpdated { get; set; }
    }

    public class Period
    {
        public string start { get; set; }
    }

    public class Practitioner
    {
        public string reference { get; set; }
        public string display { get; set; }
        public string type { get; set; }
    }

    public class Coding
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }

    public class Code
    {
        public List<Coding> coding { get; set; }
    }

    public class Coding2
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }

    public class Specialty
    {
        public List<Coding2> coding { get; set; }
    }

    public class Location
    {
        public string reference { get; set; }
        public string display { get; set; }
    }

    public class Period2
    {
        public string start { get; set; }
    }

    public class Telecom
    {
        public string system { get; set; }
        public string value { get; set; }
        public Period2 period { get; set; }
    }

    public class Resource
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta2 meta { get; set; }
        public bool active { get; set; }
        public Period period { get; set; }
        public Practitioner practitioner { get; set; }
        public List<Code> code { get; set; }
        public List<Specialty> specialty { get; set; }
        public List<Location> location { get; set; }
        public List<Telecom> telecom { get; set; }
    }

    public class Search
    {
        public string mode { get; set; }
    }

    public class Entry
    {
        public string fullUrl { get; set; }
        public Resource resource { get; set; }
        public Search search { get; set; }
    }

    public class PractitionerRoleModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta meta { get; set; }
        public string type { get; set; }
        public List<Link> link { get; set; }
        public List<Entry> entry { get; set; }
    }
}
