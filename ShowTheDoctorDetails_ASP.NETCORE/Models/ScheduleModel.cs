﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowTheDoctorDetails.Models.Schedule
{
    public class Meta
    {
        public DateTime lastUpdated { get; set; }
    }

    public class Link
    {
        public string relation { get; set; }
        public string url { get; set; }
    }

    public class Meta2
    {
        public string versionId { get; set; }
        public DateTime lastUpdated { get; set; }
    }

    public class Coding
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }

    public class ServiceCategory
    {
        public List<Coding> coding { get; set; }
    }

    public class Actor
    {
        public string reference { get; set; }
        public string type { get; set; }
        public string display { get; set; }
    }

    public class PlanningHorizon
    {
        public string start { get; set; }
    }

    public class Resource
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta2 meta { get; set; }
        public bool active { get; set; }
        public List<ServiceCategory> serviceCategory { get; set; }
        public List<Actor> actor { get; set; }
        public PlanningHorizon planningHorizon { get; set; }
    }

    public class Search
    {
        public string mode { get; set; }
    }

    public class Entry
    {
        public string fullUrl { get; set; }
        public Resource resource { get; set; }
        public Search search { get; set; }
    }

    public class ScheduleModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta meta { get; set; }
        public string type { get; set; }
        public List<Link> link { get; set; }
        public List<Entry> entry { get; set; }
    }
}
