﻿using ShowTheDoctorDetails.Models.Practitioner;
using ShowTheDoctorDetails.Models.PractitionerRole;
using ShowTheDoctorDetails.Models.Slot;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShowTheDoctorDetails.Models
{
    class MainModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string phone_no { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public List<string> specialty { get; set; }
        public List<Slots> slots { get; set; }
    }
    class Slots
    {
        public string status { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
    

}
