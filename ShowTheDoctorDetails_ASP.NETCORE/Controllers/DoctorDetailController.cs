﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShowTheDoctorDetails.Models;
using ShowTheDoctorDetails.Models.Practitioner;
using ShowTheDoctorDetails.Models.PractitionerRole;
using ShowTheDoctorDetails.Models.Schedule;
using ShowTheDoctorDetails.Models.Slot;
using Newtonsoft.Json;
namespace ShowTheDoctorDetails_ASP.NETCORE.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DoctorDetailController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<DoctorDetailController> _logger;

        public DoctorDetailController(ILogger<DoctorDetailController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            var location = System.IO.File.ReadAllText(@"E:\Miracles\fetchallthepatients\ShowTheDoctorDetails\DATA\Location.json");
            var practitioner = System.IO.File.ReadAllText(@"E:\Miracles\fetchallthepatients\ShowTheDoctorDetails\DATA\Practitioner.json");
            var practitionerRole = System.IO.File.ReadAllText(@"E:\Miracles\fetchallthepatients\ShowTheDoctorDetails\DATA\PractitionerRole.json");
            var schedule = System.IO.File.ReadAllText(@"E:\Miracles\fetchallthepatients\ShowTheDoctorDetails\DATA\Schedule.json");
            var slot = System.IO.File.ReadAllText(@"E:\Miracles\fetchallthepatients\ShowTheDoctorDetails\DATA\Slot.json");

            var locationResource = Newtonsoft.Json.JsonConvert.DeserializeObject<LocaltionModel>(location).entry.Select(o => o.resource).ToList();

            var practitionerResource = Newtonsoft.Json.JsonConvert.DeserializeObject<PactitionerModel>(practitioner).entry.Select(o => o.resource).ToList();

            var practitionerRoleResource = Newtonsoft.Json.JsonConvert.DeserializeObject<PractitionerRoleModel>(practitionerRole).entry.Select(o => o.resource).ToList();

            var scheduleModelResource = Newtonsoft.Json.JsonConvert.DeserializeObject<ScheduleModel>(schedule).entry.Select(o => o.resource).ToList();

            var slotModelResource = Newtonsoft.Json.JsonConvert.DeserializeObject<SlotModel>(slot).entry.Select(o => o.resource).ToList();

            List<MainModel> listmainModel = new List<MainModel>();

            var listPractitionerID = practitionerResource.Select(o => o.id).ToList();

            foreach (var item in listPractitionerID)
            {
                var getPractitioner = practitionerResource.Where(o => o.id == item).FirstOrDefault();
                var getName = getPractitioner.name.FirstOrDefault();
                var getPracRole = practitionerRoleResource.Where(o => o.practitioner.reference == ("Practitioner/" + item)).ToList();
                List<string> listPracRoleID = new List<string>();
                listPracRoleID.Clear();
                if (getPracRole.Count > 0)
                {
                    foreach (var pr in getPracRole)
                    {
                        listPracRoleID.Add(pr.id);
                    }
                }
                else
                    continue;

                var listScheduleId = new List<string>();
                listScheduleId.Clear();
                foreach (var i in scheduleModelResource)
                {
                    bool check;
                    foreach (var prID in listPracRoleID)
                    {
                        check = i.actor.Any(o => o.reference == ("PractitionerRole/" + prID));
                        if (check)
                        {
                            listScheduleId.Add(i.id);
                        }
                        else continue;
                    }

                }
                if (!(listScheduleId.Count > 0))
                {
                    continue;
                }

                List<Slots> listSlots = new List<Slots>();
                listSlots.Clear();

                foreach (var id in listScheduleId)
                {
                    bool check;
                    var listslot = slotModelResource.Where(o => o.schedule.reference == ("Schedule/" + id)).ToList();
                    foreach (var ls in listslot)
                    {
                        listSlots.Add(new Slots
                        {
                            status = ls.status,
                            start = ls.start,
                            end = ls.end
                        });
                    }
                }

                List<string> listspecialty = new List<string>();
                listspecialty.Clear();
                foreach (var pr in getPracRole)
                {
                    foreach (var sp in pr.specialty)
                    {
                        foreach (var cd in sp.coding)
                        {
                            listspecialty.Add(cd.display);
                        }
                    }
                }

                listmainModel.Add(new MainModel
                {
                    id = item,
                    name = getName.family + " " + getName.given[0],
                    email = getPractitioner.telecom.Where(o => o.system == "email").FirstOrDefault().value,
                    gender = getPractitioner.gender,
                    phone_no = getPractitioner.telecom.Where(o => o.system == "sms" || o.system == "phone").FirstOrDefault().value,
                    slots = listSlots,
                    specialty = listspecialty
                });
            }
            string json = JsonConvert.SerializeObject(listmainModel, Formatting.Indented);
            return json;
        }
    }
}
