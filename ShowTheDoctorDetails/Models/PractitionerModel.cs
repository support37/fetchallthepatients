﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowTheDoctorDetails.Models.Practitioner
{
    public class Meta
    {
        public DateTime lastUpdated { get; set; }
    }

    public class Link
    {
        public string relation { get; set; }
        public string url { get; set; }
    }

    public class Meta2
    {
        public string versionId { get; set; }
        public DateTime lastUpdated { get; set; }
    }

    public class Period
    {
        public string start { get; set; }
    }

    public class Name
    {
        public string use { get; set; }
        public string family { get; set; }
        public List<string> given { get; set; }
        public Period period { get; set; }
        public string text { get; set; }
    }

    public class Period2
    {
        public string start { get; set; }
    }

    public class Telecom
    {
        public string system { get; set; }
        public string value { get; set; }
        public Period2 period { get; set; }
    }

    public class Period3
    {
        public string start { get; set; }
    }

    public class Address
    {
        public string use { get; set; }
        public string type { get; set; }
        public List<string> line { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public Period3 period { get; set; }
    }

    public class Coding
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }

    public class Communication
    {
        public List<Coding> coding { get; set; }
    }

    public class Resource
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta2 meta { get; set; }
        public List<Name> name { get; set; }
        public List<Telecom> telecom { get; set; }
        public List<Address> address { get; set; }
        public string gender { get; set; }
        public string birthDate { get; set; }
        public List<Communication> communication { get; set; }
    }

    public class Search
    {
        public string mode { get; set; }
    }

    public class Entry
    {
        public string fullUrl { get; set; }
        public Resource resource { get; set; }
        public Search search { get; set; }
    }

    public class PactitionerModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta meta { get; set; }
        public string type { get; set; }
        public List<Link> link { get; set; }
        public List<Entry> entry { get; set; }
    }
}
