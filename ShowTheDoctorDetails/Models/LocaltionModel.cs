﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShowTheDoctorDetails.Models
{
    public class Meta
    {
        public DateTime lastUpdated { get; set; }
    }

    public class Link
    {
        public string relation { get; set; }
        public string url { get; set; }
    }

    public class Meta2
    {
        public string versionId { get; set; }
        public DateTime lastUpdated { get; set; }
    }

    public class Coding
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }

    public class Type
    {
        public List<Coding> coding { get; set; }
    }

    public class Period
    {
        public string start { get; set; }
    }

    public class Telecom
    {
        public string system { get; set; }
        public string value { get; set; }
        public Period period { get; set; }
    }

    public class Address
    {
        public string type { get; set; }
        public List<string> line { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
    }

    public class Coding2
    {
        public string system { get; set; }
        public string code { get; set; }
        public string display { get; set; }
    }

    public class PhysicalType
    {
        public List<Coding2> coding { get; set; }
    }

    public class Position
    {
        public double longitude { get; set; }
        public double latitude { get; set; }
    }

    public class ManagingOrganization
    {
        public string reference { get; set; }
        public string display { get; set; }
    }

    public class Resource
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta2 meta { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string mode { get; set; }
        public List<Type> type { get; set; }
        public List<Telecom> telecom { get; set; }
        public Address address { get; set; }
        public PhysicalType physicalType { get; set; }
        public Position position { get; set; }
        public ManagingOrganization managingOrganization { get; set; }
    }

    public class Search
    {
        public string mode { get; set; }
    }

    public class Entry
    {
        public string fullUrl { get; set; }
        public Resource resource { get; set; }
        public Search search { get; set; }
    }

    public class LocaltionModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public Meta meta { get; set; }
        public string type { get; set; }
        public List<Link> link { get; set; }
        public List<Entry> entry { get; set; }
    }
}
