﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fetchallthepatients.Models
{
    public class ResourceModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public object meta { get; set; }
        public object text { get; set; }
        public object[] extension { get; set; }
        public object[] identifier { get; set; }
        public object name { get; set; }
        public object[] telecom { get; set; }
        public string gender { get; set; }
        public string birthDate { get; set; }
        public object[] address { get; set; }
        public object maritalStatus { get; set; }
        public string multipleBirthBoolean { get; set; }
        public object[] communication { get; set; }
    }
}
