﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fetchallthepatients.Models
{
    public class BundleModel
    {
        public string resourceType { get; set; }
        public string id { get; set; }
        public object meta { get; set; }
        public string type { get; set; }
        public int total { get; set; }
        public object[] link { get; set; }
        public EntryModel[] entry { get; set; }
    }
}
