﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fetchallthepatients.Models
{
    public class EntryModel
    {
        public string fullUrl { get; set; }
        public ResourceModel resource { get; set; }
        public object search { get; set; }
    }
}
